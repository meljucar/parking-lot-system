package com.park.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.park.entity.EntryPoints;

public interface EntryPointsDao extends JpaRepository<EntryPoints, Integer>{

}
