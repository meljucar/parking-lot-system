package com.park.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.park.entity.ParkingSLot;

public interface ParkingSlotDao extends JpaRepository<ParkingSLot, Integer> {

}
