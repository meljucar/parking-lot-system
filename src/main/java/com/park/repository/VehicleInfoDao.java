package com.park.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.park.entity.VehicleInfo;

public interface VehicleInfoDao extends JpaRepository<VehicleInfo, Integer>{
	

}
