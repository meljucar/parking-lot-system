package com.park.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.park.entity.ParkingInfo;

public interface ParkingInfoDao extends JpaRepository<ParkingInfo, Integer>{

}
