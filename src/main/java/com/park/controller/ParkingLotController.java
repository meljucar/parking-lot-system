package com.park.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.park.entity.ParkingInfo;
import com.park.entity.VehicleInfo;
import com.park.service.ParkingLotSystemService;

@RestController
@RequestMapping("parking")
public class ParkingLotController {
	@Autowired
	private ParkingLotSystemService parkingLotSystemService;
	
	@GetMapping("/lot/{id}")
	public ResponseEntity<ParkingInfo> getParkingLot(@PathVariable Integer id) {
		return new ResponseEntity<>(parkingLotSystemService.findParkingInfoById(id), HttpStatus.OK);
	}
	
	@PostMapping("/createParkingLot")
	public ResponseEntity<ParkingInfo> createParking(@RequestBody ParkingInfo parkingInfo) {
		return new ResponseEntity<>(parkingLotSystemService.createParking(parkingInfo), HttpStatus.CREATED);
	}
	
	@PostMapping("/entry/{entryId}/{slotId}")
	public ResponseEntity<?> processVehicleEntry(@RequestBody VehicleInfo vehicle, @PathVariable Integer entryId,
			@PathVariable Integer slotId) {
		vehicle = parkingLotSystemService.processVehicleEntry(vehicle, entryId, slotId);
		
		return new ResponseEntity<>(vehicle, HttpStatus.CREATED);
	}
	
	@PutMapping("/exit/{id}/{slotId}")
	public ResponseEntity<?> processVehicleExit(@PathVariable Integer id, @PathVariable Integer slotId) {
		VehicleInfo vehicle = parkingLotSystemService.processVehicleExit(id, slotId);
		return new ResponseEntity<>(vehicle, HttpStatus.CREATED);
	}



}
