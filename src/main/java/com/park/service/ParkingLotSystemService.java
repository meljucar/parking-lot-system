package com.park.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.park.entity.EntryPoints;
import com.park.entity.ParkingInfo;
import com.park.entity.ParkingSLot;
import com.park.entity.VehicleInfo;
import com.park.repository.EntryPointsDao;
import com.park.repository.ParkingInfoDao;
import com.park.repository.ParkingSlotDao;
import com.park.repository.VehicleInfoDao;

@Service
public class ParkingLotSystemService {
	@Autowired
	private EntryPointsDao entryDao;

	@Autowired
	private ParkingSlotDao parkingSlotDao;

	@Autowired
	private VehicleInfoDao vehicleInfoDao;

	@Autowired
	private ParkingInfoDao parkingInfoDao;

	public EntryPoints findGateById(Integer id) {
		EntryPoints gate = null;

		Optional<EntryPoints> optionalGate = entryDao.findById(id);
		if (optionalGate.isPresent())
			gate = optionalGate.get();
		else
			throw new RuntimeException("No Gate Found for " + id);

		return gate;
	}

	public List<EntryPoints> findAllEntryPoints() {
		return entryDao.findAll();
	}

	public EntryPoints creategate(EntryPoints gate) {
		entryDao.save(gate);
		return gate;
	}

	public void deleteGate(Integer id) {
		findGateById(id);
		entryDao.deleteById(id);
	}

	public ParkingSLot findParkingSlotById(Integer id) {
		ParkingSLot pSlot = null;

		Optional<ParkingSLot> optionalPSlot = parkingSlotDao.findById(id);
		if (optionalPSlot.isPresent())
			pSlot = optionalPSlot.get();
		else
			throw new RuntimeException("No Parking SLot Found for " + id);

		return pSlot;
	}

	public ParkingInfo findParkingInfoById(Integer id) {
		ParkingInfo pInfo = null;

		Optional<ParkingInfo> optionalPInfo = parkingInfoDao.findById(id);
		if (optionalPInfo.isPresent())
			pInfo = optionalPInfo.get();
		else
			throw new RuntimeException("No Parking Info Found for " + id);

		return pInfo;
	}

	public void saveParkingSlot(Set<ParkingSLot> set) {

		for (ParkingSLot s : set) {
			parkingSlotDao.save(s);
		}
	}

	public VehicleInfo findVehicleInfo(Integer id) {
		VehicleInfo vehicleInfo = null;

		Optional<VehicleInfo> optionalVehicle = vehicleInfoDao.findById(id);
		if (optionalVehicle.isPresent())
			vehicleInfo = optionalVehicle.get();
		else
			throw new RuntimeException("No Vehicle Found for " + id);

		return vehicleInfo;
	}

	public VehicleInfo processVehicleEntry(VehicleInfo vehicleInfo, Integer entryPoints, Integer parkingSLot) {
		
		ParkingSLot fetchSlot = findParkingSlotById(parkingSLot);

		if (fetchSlot.isAvailable()) {

			fetchSlot.setAvailable(false);
			parkingSlotDao.save(fetchSlot);

			vehicleInfo.setEntry_dttm(LocalDateTime.now());

			vehicleInfoDao.save(vehicleInfo);
			
		}

		return vehicleInfo;

	}

	public VehicleInfo processVehicleExit(Integer vehicleId, Integer slotId) {

		VehicleInfo fetchInfo = findVehicleInfo(vehicleId);
		ParkingSLot fetchSlot = findParkingSlotById(slotId);

		LocalDateTime entryTime = fetchInfo.getEntry_dttm();
		LocalDateTime exitTime = LocalDateTime.now();

		Duration duration = Duration.between(entryTime, exitTime);

		// calculate the amount
		Double totalAmount = calculationFees(fetchInfo.getType(), duration);

		fetchInfo.setExit_dttm(exitTime);
		fetchInfo.setAmount(totalAmount);
		vehicleInfoDao.save(fetchInfo);

		fetchSlot.setAvailable(true);

		parkingSlotDao.save(fetchSlot);

		return fetchInfo;
	}

	private int getRatePerVehicleType(String type) {
		switch (type) {
		case "5":
			return 20;
		case "M":
			return 40;
		case "L":
			return 100;
		default:
			System.out.println("invalid");
		}
		return 0;
	}

	private Double calculationFees(String type, Duration duration) {
		Double totalFees = 0.0;
		int totalHrs = (int) Math.round(duration.toHours());

		int rate = getRatePerVehicleType(type);

		if (totalHrs > 3 && totalHrs < 24) {
			totalHrs = totalHrs - 3;
			totalFees = (double) (totalHrs * rate);
		} else if (totalHrs >= 24) {
			totalHrs = totalHrs - 24;
			int exceed = totalHrs * rate;
			totalFees = (double) (5000 + exceed);

		} else {
			totalFees = (double) (1 * rate);
		}
		return totalFees;
	}



	public ParkingInfo createParking(ParkingInfo parkingInfo) {
		parkingInfoDao.save(parkingInfo);

		parkingSlotDao.saveAll(parkingInfo.getParkingSlot());
		entryDao.saveAll(parkingInfo.getEntryPoints());

		return parkingInfo;
	}

}
