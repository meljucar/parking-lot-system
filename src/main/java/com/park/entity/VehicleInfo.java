package com.park.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleInfo {

	@Id
	private Integer id;

	private String plateNumber;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entrypt_id")
	private EntryPoints entryPoints;

	private String type;

	private LocalDateTime entry_dttm;

	private LocalDateTime exit_dttm;

	private Double amount;

}
